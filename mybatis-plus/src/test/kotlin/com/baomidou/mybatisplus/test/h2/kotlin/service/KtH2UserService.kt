package com.baomidou.mybatisplus.test.h2.kotlin.service

import com.baomidou.mybatisplus.extension.service.IService
import com.baomidou.mybatisplus.test.h2.kotlin.entity.KtH2User

interface KtH2UserService : IService<KtH2User> {
}
